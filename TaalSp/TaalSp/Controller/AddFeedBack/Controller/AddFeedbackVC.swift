//
//  AddFeedbackVC.swift
//  Taal
//
//  Created by Vishal on 16/06/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit

class AddFeedbackVC: UIViewController {

    //Variables
    
    lazy var mainView : AddFeedbackView = {[unowned self] in
        return self.view as! AddFeedbackView
    }()
    
    lazy var mainModelView: AddFeedbackViewModel = {
        return AddFeedbackViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theDelegate: self)
        setupNavigationbarwithBackButton(titleText: "Feedback_key".localized.capitalized, barColor: .appThemeBlueColor)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if self.mainView.txtVwFeedback.text.trimmingCharacters(in: .whitespaces).isEmpty {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else {
            let param = ["provider_id":"\(getUserData()?.user?.id ?? 0)",
                "feedback":"\(self.mainView.txtVwFeedback.text!)"]
            
            self.mainModelView.addFeedbackAPI(param: param) {
                print("Sucess")
                self.mainView.txtVwFeedback.resignFirstResponder()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension AddFeedbackVC : UITextViewDelegate {
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

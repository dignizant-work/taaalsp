//
//  AddFeedbackViewModel.swift
//  Taal
//
//  Created by Vishal on 16/06/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON

class AddFeedbackViewModel {
    
    fileprivate weak var theController: AddFeedbackVC!
    
    init(theController: AddFeedbackVC) {
        self.theController = theController
    }
    
    func addFeedbackAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        let url = addFeedbackURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        print("URL: ", url)
        print("Header: ", header)
        print("PARAM: ", param)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response {
                    let jsonData = JSON(dict)
                    print("JsonData:-\(jsonData)")
                    makeToast(strMessage: jsonData["message"].stringValue)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if statusCode == notFound {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
    
}

//
//  AddressesVC.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class AddressesVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AddressesView = { [unowned self] in
        return self.view as! AddressesView
    }()
    
    lazy var mainViewModel: AddressesViewModel = {
        return AddressesViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.setUpUI(theController: self)
        setupNavigationbarwithBackButton(titleText: "Addresses_key".localized, barColor: .appThemeBlueColor)
        self.navigationRightBtnSetUP()
    }
    
    func navigationRightBtnSetUP() {
        let btnRightAdd = UIBarButtonItem(image: UIImage(named: "ic_header_add"), style: .plain, target: self, action: #selector(btnAddAction))
        btnRightAdd.tintColor = .white
        self.navigationItem.rightBarButtonItem = btnRightAdd
    }
    
    @objc func btnAddAction() {
        let addAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        
        self.navigationController?.pushViewController(addAddressVC, animated: true)
    }
}

//
//  AddressesVC_UITableViewDelegate.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
//import SwipeCellKit

extension AddressesVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddressesTableCell", for: indexPath) as! AddressesTableCell
//        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let addAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
        
        self.navigationController?.pushViewController(addAddressVC, animated: true)
    }
    /*
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit =  UIContextualAction(style: .normal, title: "Edit", handler: { (action,view,completionHandler ) in
            //do stuff
            print("Edit")
            
            let addressesVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddressVC") as! AddAddressVC
            self.navigationController?.pushViewController(addressesVC, animated: true)
        })
        edit.image = UIImage(named: "ic_edit")
        edit.backgroundColor = UIColor.red
        
        let delete = UIContextualAction(style: .normal, title: "Delete", handler: { (action,view,completionHandler ) in
            //do stuff
            print("Delete")
        })
        delete.image = UIImage(named: "ic_delete")
        delete.backgroundColor = UIColor.red
        
        let confrigation = UISwipeActionsConfiguration(actions: [delete, edit])

        return confrigation
    }
     */
}

//
//  AddAddressView.swift
//  Taal
//
//  Created by vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import DropDown

class AddAddressView:UIView{
    //MARK: Outlets
    @IBOutlet weak var lblAddressName:UILabel!
    @IBOutlet weak var txtAddressName: UITextField!
    @IBOutlet weak var lblGovernorate:UILabel!
    @IBOutlet weak var txtGovernorate: UITextField!
    @IBOutlet weak var lblBlock: UILabel!
    @IBOutlet weak var txtBlock: UITextField!
    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var txtCity: UITextField!
    @IBOutlet weak var lblStreet: UILabel!
    @IBOutlet weak var txtStreet: UITextField!
    @IBOutlet weak var lblAvenu: UILabel!
    @IBOutlet weak var txtAvenu: UITextField!
    @IBOutlet weak var lblBuilding: UILabel!
    @IBOutlet weak var txtBuilding: UITextField!
    @IBOutlet weak var lblFloor: UILabel!
    @IBOutlet weak var txtFloor: UITextField!
    @IBOutlet weak var lblExtraNotes: UILabel!
    @IBOutlet weak var txViewExtraNotes: UITextView!
    
    func setUpUI(theController: AddAddressVC) {
        [lblAddressName,lblGovernorate,lblBlock,lblCity,lblStreet,lblAvenu,lblBuilding,lblFloor,lblExtraNotes].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        [txtAddressName,txtGovernorate,txtBlock,txtCity,txtStreet,txtAvenu,txtBuilding,txtFloor].forEach { (txt) in
            txt?.font = themeFont(size: 16, fontname: .regular)
            txt?.delegate = theController
        }
        txViewExtraNotes.font = themeFont(size: 16, fontname: .regular)
        theController.mainViewModel.arrayCityName = ["Ahemdabad","Baroda","Surat","Navsari","Mumbai"]
        theController.mainViewModel.cityNameDropDown.customCellConfiguration = { index, string, cell in
            cell.optionLabel.textAlignment = .left
        }
        theController.mainViewModel.cityNameDropDown.dataSource = theController.mainViewModel.arrayCityName
        theController.configuDropDown(dropDown: theController.mainViewModel.cityNameDropDown, sender: txtCity)
        theController.mainViewModel.cityNameDropDown.selectionAction = { (index, item) in
            self.txtCity.text = item
            theController.view.endEditing(true)
            theController.mainViewModel.cityNameDropDown.hide()
        }
    }
    
}

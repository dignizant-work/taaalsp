//
//  AddressesTableCell.swift
//  Taal
//
//  Created by Vishal on 28/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
//import SwipeCellKit

class AddressesTableCell: UITableViewCell {

    //MARK: Outlets
    @IBOutlet var lblAddressTitle: UILabel!
    @IBOutlet var lblAddressValue: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setUpUI()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setUpUI() {
        [lblAddressTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .regular)
            lbl?.textColor = UIColor.appThemeBlueColor
        }
        
        [lblAddressValue].forEach { (lbl) in
            lbl?.font = themeFont(size: 12, fontname: .light)
            lbl?.textColor = UIColor.appThemeLightBlueColor
        }
    }
    
}

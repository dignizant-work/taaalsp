//
//  CategoriesModel.swift
//  TaalSp
//
//  Created by Jaydeep on 29/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class AddCategoriesModel: NSObject {

    //MARK:- Variable
    fileprivate weak var theController:AddCategoriesVC!
//    var arrayCategoryList : [TourDataModel] = []
    var arrayProviderCategory : [TourDataModel] = []
    var arrayCategoryID = NSMutableArray()
    
    
    //MARK:- LifeCycle
    init(theController:AddCategoriesVC) {
        self.theController = theController
    }
    
}

//MARK: Categories list
extension AddCategoriesModel {
    
    func providerCategoriesListApi(completionHandler: @escaping ()-> Void) {
        
        let url = providerCategoryURL
        let parameters = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            
            self.arrayProviderCategory = []
            if statusCode == success {
                if let data = result as? NSArray {
                    let modelData = JSON(data)
                    
                    self.arrayProviderCategory = []
                    for data in modelData.arrayValue {
                        print("Data:-\(data)")
                        let categori = TourDataModel(JSON: data.dictionaryObject!)
                        self.arrayProviderCategory.append(categori!)
                    }
                    completionHandler()
                }
                else if error != nil {
                    completionHandler()
                    makeToast(strMessage: error!)
                }
            }
            else if statusCode == notFound {
                completionHandler()
                makeToast(strMessage: "Category_not_found_key".localized)
            }
            
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }
    
    func addCategoriesApi(param: [String:Any], completionHandler: @escaping ()-> Void) {
        
        let url = addCategoriesURL
        print("param: ",param)
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().makePostApiWithBody(name: url, params: param, headers: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                
                if let dict = result {
                    let msg = dict["message"] as! String
                    makeToast(strMessage: msg)
                }
                completionHandler()
            }
            else if error != nil {
                makeToast(strMessage: error!)
            }
            
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }
    
    /// Delete category
    func deleteCategoryApi(categoryID: Int, completionHandlor: @escaping() -> Void) {
        let url = deleteCategoryURL+"\(categoryID)"
        let parameters = NSDictionary() as! [String : Any]

        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        WebServices().makeDeleteApiWithBody(name: url, params: parameters, headers: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            
            if statusCode == success {
                print("Response:- ", response)
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }
}

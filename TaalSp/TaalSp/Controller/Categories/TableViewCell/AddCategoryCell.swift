//
//  AddCategoryCell.swift
//  TaalSp
//
//  Created by Jaydeep on 29/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class AddCategoryCell: UITableViewCell {
    
    //MARK:-Outlet Zone
    @IBOutlet weak var lblCategoryName: UILabel!
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblCategoryName.textColor = .appThemeBlueColor
        lblCategoryName.font = themeFont(size: 20, fontname: .regular)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

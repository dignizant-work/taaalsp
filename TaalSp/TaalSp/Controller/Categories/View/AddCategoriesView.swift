//
//  CategoriesView.swift
//  TaalSp
//
//  Created by Jaydeep on 29/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class AddCategoriesView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblCategoriesTitle: UILabel!
    @IBOutlet weak var lblEditStatus: UILabel!
    @IBOutlet var btnEdit: CustomButton!
    @IBOutlet weak var tblCategories: UITableView!
    @IBOutlet weak var lblAddToCategoryTitle: UILabel!
    @IBOutlet weak var btnAddCategory: UIButton!
    
    @IBOutlet var vwAddCategory: UIView!
    @IBOutlet var btnHideAddCategoryVw: UIButton!
    @IBOutlet var tblAddCategory: UITableView!
    @IBOutlet var tblAddCategoryHeightConstraint: NSLayoutConstraint!
    
    //MARK:- SetupUI
    
    func setupUI(theDelegate:AddCategoriesVC){
        lblCategoriesTitle.textColor = .appThemeBlueColor
        lblCategoriesTitle.font = themeFont(size: 20, fontname: .semi)
        lblCategoriesTitle.text = "Categories_key".localized
        
        lblEditStatus.textColor = .lightGray
        lblEditStatus.font = themeFont(size: 15, fontname: .semi)
        lblEditStatus.text = "Edit_key".localized
        btnEdit.isHidden = true
        lblEditStatus.isHidden = true
        
        lblAddToCategoryTitle.textColor = .appThemeBlueColor
        lblAddToCategoryTitle.font = themeFont(size: 20, fontname: .semi)
        lblAddToCategoryTitle.text = "Add_a_category_key".localized
        
        tblCategories.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellReuseIdentifier: "CategoriesCell")
        tblAddCategory.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellReuseIdentifier: "CategoriesCell")
        tblCategories.tableFooterView = UIView()
        tblCategories.delegate = theDelegate
        tblCategories.dataSource = theDelegate
        
        self.vwAddCategory.isHidden = true
        tblAddCategory.layer.borderColor = UIColor.black.cgColor
        tblAddCategory.layer.borderWidth = 1.0
        tblAddCategory.layer.cornerRadius = 5.0
        tblAddCategoryHeightConstraint.constant = 60
    }
    
}

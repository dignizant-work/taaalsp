//
//  ChatDetailsVC.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper
import SDWebImage
import FirebaseDatabase
import UIKit
//import IQKeyboardManagerSwift

class ChatDetailsVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ChatDetailsView = { [unowned self] in
        return self.view as! ChatDetailsView
        }()
    
    lazy var mainModelView: ChatDetailsViewModel = {
        return ChatDetailsViewModel(theController: self)
    }()
    
    var ref: DatabaseReference!
    var isFirstTime:Bool = true
    var handlerUpdateChatList:(Chat,String) -> Void = {_,_ in}
        
    //MARK:- ViewLifeCycke
        
    override func viewDidLoad() {
        super.viewDidLoad()

        
        ref = Database.database().reference()
        updateChatList()
        
        mainView.setUpUI(theDelegate: self)
        mainView.registertXIb(theDelegate: self)

        if self.mainModelView.conversationId != "" {
            self.mainModelView.chatHistory(userID: self.mainModelView.conversationId) {
                self.mainView.tblMessageChat.reloadData()
                print("chat data get")
                print("Array count: ", self.mainModelView.arrayChatData.count)
                self.scrollToBottom()                
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.endEditing(true)
        //Status Bar Color set
        UINavigationBar.appearance().barStyle = .blackTranslucent
        setupNavigationbarwithBackButton(titleText: mainModelView.userFullName, barColor: .appThemeBlueColor)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        isOnChatScreen = true
        strOppositeUserId = self.mainModelView.conversationId
    }
    override func viewDidDisappear(_ animated: Bool) {

        isOnChatScreen = false
        strOppositeUserId = ""
        self.view.endEditing(true)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillShowNotification)
        NotificationCenter.default.removeObserver(UIResponder.keyboardWillHideNotification)
    }
    
}

//MARK:- keyboard Observer Method
extension ChatDetailsVC {
    @objc
    func keyboardWillAppear(notification: NSNotification?) {
        guard let keyboardFrame = notification?.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }
        print("keyboardFrame:=",keyboardFrame)
        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - (self.tabBarController?.tabBar.frame.size.height ?? 0.0)
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height - (self.tabBarController?.tabBar.frame.size.height ?? 0.0)
        }
        print("keyboardHeight:=",keyboardHeight)
        mainView.constrainViewTextViewBottom.constant = keyboardHeight + 50
        
        UIView.animate(withDuration: 0.5) { [weak self] in
            self?.view.layoutIfNeeded()
            self?.scrollToBottom()
        }
    }
    
    @objc
    func keyboardWillDisappear(notification: NSNotification?) {
        mainView.constrainViewTextViewBottom.constant = 0.0
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
   
}


//MARK: Firebase setup
extension ChatDetailsVC {
    
    func createRoom(){
        let strTime = DateToString(Formatter: "hh:mm a", date: Date())
        let dict = ["time":strTime,
                    "reply":"false",
                    "message":self.mainView.txtVwMessage.text] as [String : Any]
      
        ref.child("conversations").child("\(mainModelView.conversationId)").setValue(dict)
    }
    
    func updateChatList(){
        ref.child("conversations").child("\(mainModelView.conversationId)").observe(.value) { (snapshot) in
            if self.isFirstTime {
                self.isFirstTime = false
                return
            }
            
            self.setFirebaseMsg(dictMsg: JSON(snapshot.value!))
        }
    }
    
    func scrollToBottom(){
        if self.mainModelView.arrayChatData.count > 0{
            let index = self.mainModelView.arrayChatData.count - 1
            if index >= 0{
                let finalCount = self.mainModelView.arrayChatData[index].chat!.count - 1
                self.mainView.tblMessageChat.scrollsToTop = false
                self.mainView.tblMessageChat.scrollToRow(at: IndexPath(row: finalCount, section: index), at: .bottom, animated: false)
            } 
        }
    }
    
    func setFirebaseMsg(dictMsg:JSON){
        var isAlreadyDate = Bool()
        var dataIndex = Int()
//        print("snapShot \(snapshot.value)")
        for i in 0..<self.mainModelView.arrayChatData.count{
            let dict = self.mainModelView.arrayChatData[i]
            let strDate = self.stringTodate(Formatter: "dd-MM-yyyy", strDate: dict.date!)
            
            let todayDate = self.DateToString(Formatter: "dd-MM-yyyy", date: Date())
            let date = self.stringTodate(Formatter: "dd-MM-yyyy", strDate: todayDate)
            
            if strDate == date {
                isAlreadyDate = true
                dataIndex = i
                break
            }
        }
        let dictMessage = dictMsg
        if isAlreadyDate {
            let dict = self.mainModelView.arrayChatData[dataIndex]
            var arr = dict.chat
            let dictChat = Chat()
            dictChat.message = dictMessage["message"].stringValue
            dictChat.time = dictMessage["time"].stringValue
            dictChat.reply = dictMessage["reply"].boolValue
            arr?.append(dictChat)
            self.handlerUpdateChatList(dictChat,mainModelView.conversationId)
            self.mainModelView.arrayChatData[dataIndex].chat = arr
            
        } else {
            let dict = ChatDataModel()
            var arr = [Chat]()
            let dictChat = Chat()
            dictChat.message = dictMessage["message"].stringValue
            dictChat.time = dictMessage["time"].stringValue
            dictChat.reply = dictMessage["reply"].boolValue
            arr.append(dictChat)
            self.handlerUpdateChatList(dictChat,mainModelView.conversationId)
            dict.chat = arr
            dict.date = self.DateToString(Formatter: "dd-MM-yyyy", date: Date())
            self.mainModelView.arrayChatData.append(dict)
        }
        self.mainView.afterMessageSend()
        self.mainView.tblMessageChat.reloadData()
        self.scrollToBottom()
    }
}


//MARK: Button Action
extension ChatDetailsVC {
    
    @IBAction func btnSendAction(_ sender: UIButton) {
        
//        self.mainView.txtVwMessage.text.trimmingCharacters(in: .whitespaces).isEmpty
        self.mainView.activityIndicater.isHidden = false
        if sender.isUserInteractionEnabled == true {
            self.mainView.btnSendMessage.isUserInteractionEnabled = false
            print("Send Message")
            var param : [String:Any] = [:]
            param["conversation_id"] = self.mainModelView.conversationId
            param["message"] = self.mainView.txtVwMessage.text
            self.mainModelView.sendMessageToUserAPI(param: param) {
                print("Message sent.....")
//                self.isFirstTime = true
                self.createRoom()
            }
        }
        else {
            print("Message not sent")
        }
    }
}

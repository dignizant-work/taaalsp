//
//  RightChatCell.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class RightChatCell: UITableViewCell {

    @IBOutlet weak var lblMsg: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

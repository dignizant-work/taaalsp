//
//  ChooseLocationVC.swift
//  Taal
//
//  Created by Vishal on 23/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import SwiftyJSON
import Alamofire

class ChooseLocationVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: ChooseLocationView = { [unowned self] in
        return self.view as! ChooseLocationView
        }()
    
    var locationManager: CLLocationManager?
    var dicAddress = NSMutableDictionary()
    
    /*lazy var mainModelView: ChooseLocationViewModel = {
     return ChooseLocationViewModel(theController: self)
     }()*/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.mainView.setUpUi(theDelegate: self)
        self.navigationController?.navigationBar.isHidden = true
        self.mainView.vwMap.delegate = self
        self.showMap(latitude: Double("21.170240") ?? 0.0, longitude: Double("72.831062") ?? 0.0)
        setupNavigationbarwithBackButton(titleText: "Tracking_key".localized, barColor: .appThemeBlueColor)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {

        self.mainView.setUpData(theDelegate: self)
    }
    
    @objc func btnDismiss(sender: UIBarButtonItem) {
        
    }
    
    func showMap(latitude: Double, longitude: Double) {
        
        appdelegate.locationManager.delegate = self
        
        let cameraCoord = CLLocationCoordinate2D(latitude: appdelegate.lattitude, longitude: appdelegate.longitude)
        self.mainView.vwMap.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18.0)
    }
    
    //MARK:- Button Action
    @IBAction func btnChooseLocationAction(_ sender: Any) {
        
    }
    
}

//MARK: Map delegate
extension ChooseLocationVC : GMSMapViewDelegate , CLLocationManagerDelegate{
    
    func didUpdateLocation(lat: Double!, lon: Double!) {
        print("lat - ",lat)
        print("lon - ",lon)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        /*
         let origin = "\(37.778483),\(-122.513960)"
         let destination = "\(37.706753),\(-122.418677)"
         let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(origin)&destination=\(destination)&mode=driving&key=[YOUR-API-KEY]"
         Alamofire.request(url).responseJSON { response in
             let json = JSON(data: response.data!)
             let routes = json["routes"].arrayValue
             for route in routes {
                 let routeOverviewPolyline = route["overview_polyline"].dictionary
                 let points = routeOverviewPolyline?["points"]?.stringValue
                 let path = GMSPath.init(fromEncodedPath: points!)
                 let polyline = GMSPolyline(path: path)
                 polyline.strokeColor = .black
                 polyline.strokeWidth = 10.0
                 polyline.map = mapViewX
             }
         }
         */
        
        let myLocation = "\(appdelegate.lattitude),\(appdelegate.longitude)"
        let destinationLocation = "\(appdelegate.lattitude),\(appdelegate.longitude)"

        let url = "https://maps.googleapis.com/maps/api/directions/json?origin=\(myLocation)&destination=\(destinationLocation)&mode=driving&key=\(googleMapKey)"
        
        Alamofire.request(url).responseJSON { (response) in
            
            let json = try! JSON(data: response.data!)
            let routes = json["routes"].arrayValue
            
            for route in routes {
                let routeOverviewPolyline = route["overview_polyline"].dictionary
                let points = routeOverviewPolyline?["points"]?.stringValue
                let path = GMSPath.init(fromEncodedPath: points!)
                let polyline = GMSPolyline(path: path)
                polyline.strokeColor = .black
                polyline.strokeWidth = 10.0
                polyline.map = self.mainView.vwMap
            }
        }
        
//        if let latestLocation = locations.first {
//            print("lattitude: ", latestLocation.coordinate.latitude)
//            print("longitude: ", latestLocation.coordinate.longitude)
//
//            DispatchQueue.main.async {
//                let marker = GMSMarker()
//                marker.position = CLLocationCoordinate2D(latitude: appdelegate.lattitude, longitude: appdelegate.longitude)
//                marker.map = self.mainView.vwMap
//            }
//        }
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {

        print("Lat:", position.target.latitude)
        print("Lat:", position.target.longitude)
        
        currentLocationTitle(lat: position.target.latitude, long: position.target.longitude)
    }
    
    func currentLocationTitle(lat: Double,long: Double)  {
        
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: lat , longitude: long)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            print("Response GeoLocation :", placemarks ?? "")
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            if placeMark != nil {
                // Country
                guard let country = placeMark.addressDictionary?["Country"] as? String else { return }
                print("Country :- \(country)")
                // City
                guard let city = placeMark.addressDictionary?["City"] as? String else { return }
                print("City :- \(city)")
                // State
                guard let state = placeMark.addressDictionary?["State"] as? String else { return }
                print("State :- \(state)")
                // Street
                // ZIP
                guard let zip = placeMark.addressDictionary?["ZIP"] as? String else { return }
                print("ZIP :- \(zip)")
                // Location name
                guard let locationName = placeMark?.addressDictionary?["Name"] as? String else { return }
                print("Location Name :- \(locationName)")
                // Street address
                //                 guard let thoroughfare = placeMark?.addressDictionary?["Thoroughfare"] as? NSString  else { return }
                //                    print("Thoroughfare :- \(thoroughfare)")
                //Street
                var streetNumber = String()
                if let street = placeMark.addressDictionary?["Street"] as? String{
                    print("Street :- \(street)")
                    let str = street
                    
                    streetNumber = str.components(
                        separatedBy: NSCharacterSet.decimalDigits.inverted).joined(separator: "")
                    
                    self.mainView.txtLocation.text = "\(streetNumber), \(locationName), \(city), \(state)-\(zip), \(country)"
                    
                    print("streetNumber :- \(streetNumber)" as Any)
                }
                
                if streetNumber == "" {
                    self.mainView.txtLocation.text = " \(locationName), \(city), \(state)-\(zip), \(country)"
                }
                
                self.dicAddress["country"] = country
                self.dicAddress["city"] = city
                self.dicAddress["state"] = state
                self.dicAddress["zip"] = zip
                self.dicAddress["locationName"] = locationName
                self.dicAddress["streetNumber"] = streetNumber
            }
        })
    }
}




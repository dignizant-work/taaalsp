//
//  CountryCodeVC.swift
//  Liber
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol CountryCodeDelegate : class {
    func CountryCodeDidFinish(data: JSON)
}

class CountryCodeVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: CountryCodeView = {
        return self.view as! CountryCodeView
    }()
    
    lazy var mainViewModel: CountryCodeViewModel = {
        return CountryCodeViewModel(theDelegate: self)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        mainView.getCode(theDelegate: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction func btnDismissContryAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
}


//MARK: - keyboard show/hide method

extension CountryCodeVC
{
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        {
            let keyboardHeight = keyboardRectValue.height
            UIView.animate(withDuration: 1.5, animations: {
                self.mainView.constantBottomTblCountry.constant = keyboardHeight-25
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        
        UIView.animate(withDuration: 1.5, animations: {
            self.mainView.constantBottomTblCountry.constant = 0.0
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
    
}



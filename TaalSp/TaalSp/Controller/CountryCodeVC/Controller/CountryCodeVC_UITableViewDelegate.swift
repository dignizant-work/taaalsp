//
//  CountryCodeVC_UITableViewDelegate.swift
//  Liber
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension CountryCodeVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return mainView.isFiltered ? mainView.filteredCountryList.count : mainView.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CountryCodeTableCell") as! CountryCodeTableCell
        
        cell.selectionStyle = .none
        let data = mainView.isFiltered ? mainView.filteredCountryList[indexPath.row] : mainView.countryList[indexPath.row]
        cell.imgCountry.image = UIImage(named: data["code"].string ?? "")
        cell.lblCode.text = data["dial_code"].string ?? ""
        let str1 = (data["name"].string ?? "") + " ("
        let str2 = (data["code"].string ?? "") + ")"
        cell.lblCountry.text = str1 + str2
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = mainView.isFiltered ? mainView.filteredCountryList[indexPath.row] : mainView.countryList[indexPath.row]
        mainView.delegate?.CountryCodeDidFinish(data: data)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func dismissScreen() {
        self.dismiss(animated: false, completion: nil)
    }
}

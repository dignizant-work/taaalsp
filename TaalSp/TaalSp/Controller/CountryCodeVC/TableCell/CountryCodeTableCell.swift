//
//  CountryCodeTableCell.swift
//  Liber
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import UIKit

class CountryCodeTableCell: UITableViewCell {

    @IBOutlet weak var imgCountry: UIImageView!
    @IBOutlet weak var lblCountry: UILabel!
    @IBOutlet weak var lblCode: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblCountry,lblCode].forEach { (lbl) in
            // txt?.placeHolderColor = UIColor.appLightGrayColor
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.black
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

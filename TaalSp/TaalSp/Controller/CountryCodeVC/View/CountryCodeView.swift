//
//  CountryCodeView.swift
//  Liber
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class CountryCodeView: UIView {
    
    //MARK: Outlets
    
    @IBOutlet weak var lblSelectCountry: UILabel!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tblCountry: UITableView!
    @IBOutlet weak var constantBottomTblCountry: NSLayoutConstraint!
    @IBOutlet var btnDismiss: UIButton!
    
    //MARK: Variables
    var countryList : JSON = JSON()
    var filteredCountryList : JSON = JSON()
    var isFiltered : Bool = false
    weak var delegate: CountryCodeDelegate?
    
    
    //MARK: SetUpUI
    func setUpUI(theDelegate: CountryCodeVC) {
        tblCountry.tableFooterView = UIView()
        lblSelectCountry.font = themeFont(size: 18.0, fontname: .medium)
        lblSelectCountry.textColor = .black
        lblSelectCountry.text = "Select_country_key".localized
    }
    
    func getCode(theDelegate: CountryCodeVC) {
        if let path = Bundle.main.path(forResource: "CountryCodes", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                print("jsonResult",JSON(jsonResult))
                countryList = JSON(jsonResult)
                self.tblCountry.reloadData()
            }  catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
    }
    
    
}

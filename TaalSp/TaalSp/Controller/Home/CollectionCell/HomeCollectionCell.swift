//
//  HomeCollectionCell.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HomeCollectionCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwOuter: CustomView!
    @IBOutlet weak var lblCategoriesName: UILabel!
    
    //MARK:- ViewLife cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblCategoriesName].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .semiBold)
            lbl?.textColor = .white
        }
    }

}

//
//  HomeVC.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseMessaging

class HomeVC: UIViewController {
    
    //MARK:- Outlet Zone    

    //MARK:- Variable
    fileprivate lazy var theCurrentView:HomeView = { [unowned self] in
        return self.view as! HomeView
        }()
    
    private lazy var theCurrentModel: HomeModel = {
        return HomeModel(theController: self)
    }()
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
            
        if getUserData()?.accessToken != nil {
                self.allProviderCategoriesListApi {
                self.theCurrentView.collectionViewCategories.reloadData()
                self.theCurrentModel.userPendingRequestsAPI({
                    self.theCurrentView.tblHome.reloadData()
                    print("Pending category count: \(self.theCurrentModel.arrayPendingCategory.count)")
                })
            }
        }
        
//        handlorNewCreateCase = {
//            self.theCurrentModel.userPendingRequestsAPI({
//                self.theCurrentView.tblHome.reloadData()
//                print("Pending category count: \(self.theCurrentModel.arrayPendingCategory.count)")
//            })
//        }
        
        self.theCurrentView.txtSearch.addTarget(self, action: #selector(textFieldEditingChanged(_:)), for: .editingChanged)
        
        hidekeyBoardWhenTappedAround()
        theCurrentView.setupUI(theController:self)
        self.setupPullToRefresh()
        checkDeviceLanguageDataApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        isHistoryVC = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        setupNavigationbarWithoutBackButton(titleText: "Home_key".localized, barColor: .appThemeBlueColor)
        self.theCurrentView.collectionViewCategories.reloadData()
        
        if isCategoryDelete {
            self.theCurrentModel.userPendingRequestsAPI({
                self.theCurrentView.tblHome.reloadData()
                isCategoryDelete = false
                print("Pending category count: \(self.theCurrentModel.arrayPendingCategory.count)")
            })
        }
        else if isJobCompleted {
            self.theCurrentModel.arrayPendingCategory = []
            self.theCurrentModel.userPendingRequestsAPI({
                self.theCurrentView.tblHome.reloadData()
                isCategoryDelete = false
                isJobCompleted = false
                print("Pending category count: \(self.theCurrentModel.arrayPendingCategory.count)")
            })
        }
    }
    
    func checkDeviceLanguageDataApi() {
        
        if getUserData()?.accessToken != nil {
            let param = ["player_id":Messaging.messaging().fcmToken ?? ""]
            self.theCurrentModel.changeLanguageAPI(param: param) {
                print("Success")
            }
        }
    }
}

extension HomeVC {
    
    func setupPullToRefresh() {
        
        self.theCurrentModel.refreshController = UIRefreshControl()
        self.theCurrentModel.refreshController.backgroundColor = UIColor.clear
        self.theCurrentModel.refreshController.tintColor = UIColor.appThemeBlueColor
        self.theCurrentModel.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.theCurrentView.tblHome.addSubview(self.theCurrentModel.refreshController)
    }
    
    @objc func pullToRefresh() {
        
//        self.allProviderCategoriesListApi {
//            self.theCurrentView.collectionViewCategories.reloadData()
//        }
        self.theCurrentView.txtSearch.text = ""
        self.theCurrentModel.userPendingRequestsAPI {
            self.theCurrentView.tblHome.reloadData()
        }
    }
}

//MARK:- Collection View Delegate

extension HomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCategoryList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
        
        let dict = arrayCategoryList[indexPath.row]
        cell.lblCategoriesName.text = dict.name
        
        if dict.isSubCat == 0 {
            cell.vwOuter.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        else {
            cell.vwOuter.backgroundColor = UIColor.appThemeBlueColor
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        guard let cell: HomeCollectionCell = Bundle.main.loadNibNamed("HomeCollectionCell",
                                                                      owner: self,
                                                                      options: nil)?.first as? HomeCollectionCell else {
                                                                        return CGSize.zero
        }
        let dict = arrayCategoryList[indexPath.row]
        cell.lblCategoriesName.text = dict.name
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: size.width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        for data in arrayCategoryList {
            data.isSubCat = 0
        }
        let dict = arrayCategoryList[indexPath.row]
        dict.isSubCat = 1
        arrayCategoryList[indexPath.row] = dict
        collectionView.reloadData()
        
        self.theCurrentModel.categoryID = "\(dict.id!)"
        self.theCurrentModel.userPendingRequestsAPI({
            self.theCurrentView.tblHome.reloadData()
            print("Pending category count: \(self.theCurrentModel.arrayPendingCategory.count)")
        })
        
    }
    
}

//MARK:- TableView Delegate

extension HomeVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if self.theCurrentModel.arrayPendingCategory.count == 0 {
            setTableView("No_record_found_key".localized, tableView: self.theCurrentView.tblHome)
            return 0
        }
        tableView.backgroundView = nil
        
        if tableView == self.theCurrentView.tblHome {
            
            return theCurrentModel.arrayPendingCategory.count
        }
        
        let array = self.theCurrentModel.arrayPendingCategory[self.theCurrentView.tblHome.tag].caseDescription.filter({ (data) -> Bool in
            
            let voice = data.type?.lowercased() != "Voice".lowercased()
            let img = data.type != "image"
            return voice && img
        })
        
        return array.count
        
//        return self.theCurrentModel.arrayPendingCategory[self.theCurrentView.tblHome.tag].caseDescription?.count ?? 0
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
//        var cell = HomeTableCell()
        
        if tableView == self.theCurrentView.tblHome {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as! HomeTableCell
            let dict = self.theCurrentModel.arrayPendingCategory[indexPath.row]
            
            cell.setUpData(data: dict)
            
            self.theCurrentView.tblHome.tag = indexPath.row
            
            cell.tblDescription.register(UINib(nibName: "CategoryDescriptionTableCell", bundle: nil), forCellReuseIdentifier: "CategoryDescriptionTableCell")
            
            cell.btnWriteYourQuatationOutlet.isUserInteractionEnabled = false
            cell.btnWriteYourQuatationOutlet.tag = indexPath.row
            cell.btnWriteYourQuatationOutlet.addTarget(self, action: #selector(btnAddQuotationAction(_:)), for: .touchUpInside)
            
            cell.tblDescription.tableFooterView = UIView()
            cell.tblDescription.delegate = self
            cell.tblDescription.dataSource = self
            cell.tblDescription.reloadData {
//                cell.tblDescription.reloadData {
//                    cell.tblDescriptionHeightConstant.constant = cell.tblDescription.contentSize.height
////                    self.tblDescriptionHeightConstant.constant = tblDescription.contentSize.height
//                }
//                cell.tblDescription.layoutIfNeeded()
//                cell.tblDescription.layoutSubviews()
            }
            cell.tblDescription.layoutIfNeeded()
            cell.tblDescription.layoutSubviews()
            cell.tblDescription.isScrollEnabled = false
            
            cell.btnViewReview.tag = indexPath.row
            cell.btnViewReview.addTarget(self, action: #selector(btnViewReviewAction(_:)), for: .touchUpInside)
            
            return cell
        }
        
        let desCell = tableView.dequeueReusableCell(withIdentifier: "CategoryDescriptionTableCell") as! CategoryDescriptionTableCell
                
        desCell.imgCategory.image = nil
        desCell.btnAudioPlayStop.addTarget(self, action: #selector(audioButtonAction(_:)), for: .touchUpInside)
        desCell.btnAudioPlayStop.isUserInteractionEnabled = true
        
        let filter = self.theCurrentModel.arrayPendingCategory[self.theCurrentView.tblHome.tag].caseDescription.filter { (dict) -> Bool in
            
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            let imgs = dict.type != "image"
            return voice && img && imgs
        }
        
        let data = filter[indexPath.row]
        desCell.setUpData(data: data)
        
//        if self.theCurrentModel.arrayPendingCategory[self.theCurrentView.tblHome.tag].caseDescription?.count ?? 0 > 0 {
//            desCell.setUpData(value: self.theCurrentModel.arrayPendingCategory[self.theCurrentView.tblHome.tag], data: desDict!)
//        }
        
        return desCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RequestDetailVC") as! RequestDetailVC

        let dict = self.theCurrentModel.arrayPendingCategory[indexPath.row]
        obj.theCurrentModel.caseID = "\(dict.caseId ?? 0)"
        obj.theCurrentModel.checkVCCome = .home
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnAddQuotationAction(_ sender: UIButton) {
        
        let dict = self.theCurrentModel.arrayPendingCategory[sender.tag]
        print("Dict: ", JSON(dict))
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WriteQuatationVC") as! WriteQuatationVC
        obj.theCurrentModel.addEditQuatation = .add
        dataCategoriData = dict
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
 
    override func viewDidLayoutSubviews() {
        self.theCurrentView.tblHome.reloadData()
    }
    
    @objc func btnViewReviewAction(_ sender: UIButton) {
        let dict = self.theCurrentModel.arrayPendingCategory[sender.tag]
        print("Data:-\(dict)")
        
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ShowReviewVC") as! ShowReviewVC
        obj.mainModelView.userID = "\(dict.userDetails?.userID ?? 0)"
        self.present(obj, animated: true, completion: nil)
    }
    
    
    @objc func audioButtonAction(_ sender: UIButton) {
        
        //
        print("Sender: \(sender.tag)")
        let audio = self.theCurrentModel.arrayPendingCategory[self.theCurrentView.tblHome.tag].caseDescription[sender.tag]
        
        if audio.label == "Voice_key".localized {
            if let data = audio.value?.first {
                print("MP3 Audio: \(data)")
            }
        }
    }
    
}

//MARK: - textField Delegate

extension HomeVC:UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    @objc func textFieldEditingChanged(_ textField: UITextField) {
        
        let searchText  = textField.text ?? ""
        if searchText.count > 0 {
            
            let array = self.theCurrentModel.arrayPendingCategory.filter { (data) -> Bool in
                let caseID = "\(data.caseId ?? 0)".lowercased().contains(searchText.lowercased())
                return caseID
            }
            self.theCurrentModel.arrayPendingCategory = array
            self.theCurrentView.tblHome.reloadData()
        }
        else {
            self.theCurrentModel.arrayPendingCategory = self.theCurrentModel.arrayAllData
            self.theCurrentView.tblHome.reloadData()
        }
    }
}

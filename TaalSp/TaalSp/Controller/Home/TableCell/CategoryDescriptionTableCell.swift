//
//  CategoryDescriptionTableCell.swift
//  TaalSp
//
//  Created by Vishal on 19/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class CategoryDescriptionTableCell: UITableViewCell {

    
    //MARK: Initializer
    @IBOutlet weak var vwLabel: UIView!
    @IBOutlet var lblDescriptionTitle: UILabel!
    @IBOutlet var lblDescriptionValue: UILabel!
    @IBOutlet weak var vwAudio: UIView!
    
    @IBOutlet weak var lblVoiceNotes: UILabel!
    @IBOutlet weak var lblTimer: UILabel!
    @IBOutlet weak var sliderTimer: UISlider!
    @IBOutlet weak var btnAudioPlayStop: UIButton!
    @IBOutlet weak var imgCategory: UIImageView!
        
    @IBOutlet weak var vwImage: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblTimer, lblDescriptionTitle, lblDescriptionValue].forEach({ (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.semi)
            lbl?.alpha = 0.5
        })
        
        [vwAudio, vwImage, vwLabel].forEach { (vw) in
            vw?.isHidden = true
        }
        
        [lblDescriptionTitle, lblDescriptionValue].forEach { (lbl) in
            lbl?.isHidden = true
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func btnAudioAction(_ sender: Any) {
        print("Selected:")
    }
    
    func setUpDetailsData(data: CaseDescription) {
        if data.label != nil && data.label == "Voice_key".localized || data.label == "images_key".localized {
            
        }
        else if data.label != nil && data.label == "images_key".localized {
            
        }
        else {
            vwLabel.isHidden = false
            [lblDescriptionTitle, lblDescriptionValue].forEach { (lbl) in
                lbl?.isHidden = false
            }
            lblDescriptionTitle.text = "\(data.label ?? "") : "
            lblDescriptionValue.text = data.value?.first
        }
    }
    
    func setUpData(data: CaseDescription) {
        
        
        
        
        if data.label != nil && data.label == "Voice_key".localized  {
//            vwAudio.isHidden = false
//            [lblDescriptionTitle, lblDescriptionValue].forEach { (lbl) in
//                lbl?.isHidden = true
//            }
        }
        else if data.label != nil && data.label == "images_key".localized {
            /*
            imgCategory.sd_setShowActivityIndicatorView(true)
            imgCategory.sd_setIndicatorStyle(.gray)
            imgCategory.sd_setImage(with: value.categoryImage?.toURL(), placeholderImage: UIImage(named: "ic_request_details_big_splash_holder"), options: .lowPriority, completed: nil)
//            imgCategory.sd_setImage(with: data.value?.first?.toURL(), placeholderImage: UIImage(named: "ic_request_details_big_splash_holder"), options: .lowPriority, completed: nil)
            vwImage.isHidden = false
            [lblDescriptionTitle, lblDescriptionValue].forEach { (lbl) in
                lbl?.isHidden = true
            }
             */
        }
        else {
            vwImage.isHidden = true
            vwAudio.isHidden = true
            vwLabel.isHidden = false
            [lblDescriptionTitle, lblDescriptionValue].forEach { (lbl) in
                lbl?.isHidden = false
            }
            lblDescriptionTitle.text = "\(data.label ?? "") : "
            lblDescriptionValue.text = data.value?.first
        }
    }
}

//
//  HomeView.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HomeView: UIView {
    
    //MARK:- ViewLifeCycle
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionViewCategories: UICollectionView!
    @IBOutlet weak var tblHome: UITableView!
    
    //MARK:- Setup
    
    func setupUI(theController:HomeVC){
       
        txtSearch.delegate = theController
        txtSearch.textColor = .black
        txtSearch.font = themeFont(size: 14, fontname: .regular)
        txtSearch.placeholder = "What_are_you_looking_for_key".localized

        collectionViewCategories.register(UINib(nibName: "HomeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionCell")
        collectionViewCategories.reloadData()
        
        tblHome.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
        
//        tblHome.reloadData()
        
    }

}

//
//  MyChatVC.swift
//  Taal
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class MyChatVC: UIViewController {
    //MARK:- Variables
    lazy var mainView: MyChatView = { [unowned self] in
        return self.view as! MyChatView
        }()
    
    lazy var mainModelView: MyChatViewModel = {
        return MyChatViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        mainView.setupUI(theDelegate: self)
        self.mainModelView.getProviderChat {
            print("Success: ")
            self.mainView.tblMyChat.reloadData()
        }
        self.refreshTableData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setupNavigationbarwithBackButton(titleText: "My_chats_key".localized.capitalized, barColor: .appThemeBlueColor)
        isNotificationChatVC()
    }

    
    func isNotificationChatVC() {
        if isNotificationChat {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
            isNotificationChat = false
            obj.mainModelView.conversationId = dictNotificationData["conversation_id"].stringValue
            obj.mainModelView.userFullName = dictNotificationData["fullname"].stringValue
            //        obj.mainModelView.dictChatModel = dict
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: false)
        }
    }
}

//MARK:
extension MyChatVC {
    
    func refreshTableData() {
        
        self.mainModelView.refreshController = UIRefreshControl()
        self.mainModelView.refreshController.backgroundColor = UIColor.clear
        self.mainModelView.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.mainView.tblMyChat.addSubview(self.mainModelView.refreshController)
    }
    
    @objc func pullToRefresh() {
        self.mainModelView.getProviderChat {
            print("Success: ")
            self.mainView.tblMyChat.reloadData()
            self.mainModelView.refreshController.endRefreshing()
        }
        
//        self.mainModelView.refreshController.endRefreshing()
    }
    
    /*
     func setupPullToRefresh() {
        
        self.theCurrentModel.refreshController = UIRefreshControl()
        self.theCurrentModel.refreshController.backgroundColor = UIColor.clear
        self.theCurrentModel.refreshController.tintColor = UIColor.appThemeBlueColor
        self.theCurrentModel.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.theCurrentView.tblHome.addSubview(self.theCurrentModel.refreshController)
    }
    
    @objc func pullToRefresh() {
        self.theCurrentModel.userPendingRequestsAPI {
            self.theCurrentView.tblHome.reloadData()
        }
    }
     */
    
}

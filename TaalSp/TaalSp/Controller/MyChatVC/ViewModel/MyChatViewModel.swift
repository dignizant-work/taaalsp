//
//  MyChatViewModel.swift
//  Taal
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MyChatViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:MyChatVC!
    var arrayChatList : [ChatDataModel] = []
    var refreshController : UIRefreshControl!
    
    
    //MARK: Initialized
    init(theController:MyChatVC) {
        self.theController = theController
    }
    
    //MARK: Variables
    var arrayHeader : [String] = []
    var currentIndex = 0
    var isOwner = false
    
    
    //MARK: ChatListAPI SetUP
    func getProviderChat(completionHandlor:@escaping()->Void) {
        
        let url = providerChatListURL
        print("URL: ", url)
        let param = NSDictionary() as! [String:Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                if let data = response as? NSArray {
                    let jsonData = JSON(data).arrayValue
                    print("JsonData: ", JSON(data))
                    
                    self.arrayChatList = []
                    for value in jsonData {
                        let chatValue = ChatDataModel(JSON: value.dictionaryObject!)
                        self.arrayChatList.append(chatValue!)
                    }
                }
                else if let data = response as? NSDictionary {
                    let jsonData = JSON(data).dictionaryObject
                    print("JsonData: ", JSON(data))
                }
                completionHandlor()
            }
            if statusCode == notFound {
//                makeToast(strMessage: "No_record_found_key".localized)
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
}

//
//  ProfileView.swift
//  Taal
//
//  Created by Jaydeep on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class ProfileView: UIView {
    
    //MARK: Outlets
    
    @IBOutlet var btnProfileSelection: UIButton!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var tblProfile: UITableView!
    @IBOutlet weak var lblEditStatus: UILabel!
    
    //MARK:- Setup UI
    
    func setupUI(theDelegate: ProfileVC) {
        imgProfile.layer.cornerRadius = imgProfile.bounds.width/2
        imgProfile.clipsToBounds = true
        
        txtName.font = themeFont(size: 18, fontname: .semi)
        txtName.textColor = .appThemeBlueColor
        
        txtEmail.font = themeFont(size: 14, fontname: .semi)
        txtEmail.textColor = .appThemeBlueColor
        txtEmail.alpha = 0.5
        
        lblEditStatus.textColor = .lightGray
        lblEditStatus.font = themeFont(size: 15, fontname: .semi)
        
//        btnUpdateAction(isUpdate: false)
        self.btnProfileSelection.isUserInteractionEnabled = false
        [txtName,txtEmail].forEach { (txtField) in
            txtField?.isUserInteractionEnabled = false
            txtField?.resignFirstResponder()
        }
        tblProfile.register(UINib(nibName: "ProfileCell", bundle: nil), forCellReuseIdentifier: "ProfileCell")
        
        tblProfile.dataSource = theDelegate
        tblProfile.delegate = theDelegate
        
        lblEditStatus.text = "Edit_key".localized
    }
    
    func btnUpdateAction(isUpdate:Bool){
        if isUpdate{
            [txtName,txtEmail].forEach { (txtField) in
                txtField?.isUserInteractionEnabled = true
            }
            lblEditStatus.text = "Update_key".localized
            txtName.becomeFirstResponder()
        } else {
            [txtName,txtEmail].forEach { (txtField) in
                txtField?.isUserInteractionEnabled = false
                txtField?.resignFirstResponder()
            }
            lblEditStatus.text = "Edit_key".localized            
        }
    }
}


//
//  RequestView.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblRequest: UITableView!
    
    //MARK:- Setup UI
    
    func setupUI(){
        
        tblRequest.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
        tblRequest.reloadData()        
    }
}

//
//  RequestImageCell.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestImageCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    @IBOutlet weak var imgRequest: UIImageView!
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

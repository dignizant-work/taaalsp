//
//  RequestDetailModel.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import AVFoundation

class RequestDetailModel: NSObject {
    
    //MARK:- Variable
    fileprivate weak var theController:RequestDetailVC!
    var arrRequestImage:[JSON] = []
    var currentIndex:Int = 0
    var audioPlayer: AVAudioPlayer?
    var audioRecording: AVAudioRecorder?
    var caseID = ""
    var categoryData: CategoryDataModel?
    var checkVCCome = enumCheckRequestDetailWhichVC.home
    var arrayImage = [String]()
    
    //MARK:- LifeCycle
    init(theController:RequestDetailVC) {
        self.theController = theController
    }
    
    //MARK: ApiSetUP
    func userCategoryDetailApi(id: String, completionHandlor:@escaping()->Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        let url = userCaseDetailURL+id
        print("URL: ", url)
        let params = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: params, header: header) { (result, error, statusCode) in
            
            if statusCode == success {
                
                if let array = result as? NSArray {
                    let data = JSON(array).arrayValue
                    print("Data: ", data)
                }
                else if let dict = result as? NSDictionary {
                    let data = JSON(dict).dictionaryObject
                    print("Data: ", data!)
                    self.categoryData = CategoryDataModel(JSON: data!)
                    dataCategoriData = CategoryDataModel(JSON: data!)
                    dataCategoriData?.quotationId = NSNumber(value: Int(self.caseID)!)
                }
                self.theController.hideLoader()
                completionHandlor()
            }
            else if error != nil {
                self.theController.hideLoader()
                makeToast(strMessage: error?.localized ?? "")
//                completionHandlor()
            }
            else {
                self.theController.hideLoader()
//                completionHandlor()
            }
        }
    }
}


//MARK: Audio SetUP
extension RequestDetailModel {
    
    func setupAudioFile() {
        theController.theCurrentView.btnPlay.isEnabled = false
        theController.theCurrentView.btnStop.isEnabled = false
        
        
        let fileMgr = FileManager.default
        let dirPaths = fileMgr.urls(for: .documentDirectory,
                                    in: .userDomainMask)
        let soundFileURL = dirPaths[0].appendingPathComponent("sound.caf")
        
        let recordSettings =
            [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
             AVEncoderBitRateKey: 16,
             AVNumberOfChannelsKey: 2,
             AVSampleRateKey: 44100.0] as [String : Any]
        
        let audioSession = AVAudioSession.sharedInstance()
        
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord, mode: .default, options: AVAudioSession.CategoryOptions.defaultToSpeaker)//(AVAudioSession.Category.playAndRecord)
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
        
        do {
            try audioRecording = AVAudioRecorder(url: soundFileURL,
                                                 settings: recordSettings as [String : AnyObject])
            audioRecording?.prepareToRecord()
        } catch let error as NSError {
            print("audioSession error: \(error.localizedDescription)")
        }
    }
}

//MARK:- Setup Array

extension RequestDetailModel {
    func setupArray(){
        arrRequestImage = []
        
        var dict = JSON()
        dict["image"] = "image"
        dict["is_selected"] = "0"
        arrRequestImage.append(dict)
       
        dict = JSON()
        dict["image"] = "image"
        dict["is_selected"] = "0"
        arrRequestImage.append(dict)
        
        dict = JSON()
        dict["image"] = "image"
        dict["is_selected"] = "0"
        arrRequestImage.append(dict)
        
        dict = JSON()
        dict["image"] = "image"
        dict["is_selected"] = "0"
        arrRequestImage.append(dict)
    }
}


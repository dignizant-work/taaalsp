//
//  RequestTableCell.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestTableCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblRequestTitle: UILabel!
    @IBOutlet weak var lblRequestNumber: UILabel!
    @IBOutlet weak var lblOfferedTitle: UILabel!
    @IBOutlet weak var lblOfferedPrice: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDesriptionDetail: UILabel!
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vwStatus: CustomView!
    @IBOutlet var btnAudio: UIButton!
    @IBOutlet weak var vwReview: UIView!
    @IBOutlet weak var btnWriteaReview: UIButton!
    @IBOutlet weak var imgArrow: UIImageView!
    
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        vwReview.isHidden = true
        [lblRequestTitle,lblRequestNumber,lblDescriptionTitle,lblStatus,lblDesriptionDetail,lblOfferedPrice].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname:.semi)
        }
        
        lblDesriptionDetail.font = themeFont(size: 12, fontname:.light)
        btnWriteaReview.titleLabel?.font = themeFont(size: 14, fontname:.semi)
        btnWriteaReview.setTitle("Write_a_review_key".localized, for: .normal)
        lblRequestTitle.text = "Request_key".localized.uppercased()
        
        [lblStatusTitle,lblOfferedTitle].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.semi)
            lbl?.alpha = 0.5
        }
        
        lblOfferedTitle.text = "\("Offered_price_key".localized) : "     
        
        lblDescriptionTitle.text = "Description".localized
        
        lblStatusTitle.text = "Status".localized
        imgArrow.image = UIImage(named: "ic_arrow")?.imageFlippedForRightToLeftLayoutDirection()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func  setUpData(data: CategoryDataModel) {
        
        self.lblDescriptionTitle.text = data.categoryName
        self.lblRequestNumber.text = "\(data.caseId ?? 0)"
        self.lblDesriptionDetail.text = data.extraNotes
        self.lblOfferedPrice.text = data.offeredPrice + " KD"
        self.lblStatus.text = data.status
        
        if data.status.lowercased() == "Completed_key".localized.lowercased() {
            vwStatus.isHidden = true
            vwReview.isHidden = false
        }
        
//        if data.voiceNotes == "" {
//            self.btnAudio.isHidden = true
//        }
//        else {
//            self.btnAudio.isHidden = false
//        }
        self.btnAudio.isHidden = true
    }
}

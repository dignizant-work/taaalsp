//
//  RequestHistoryVC.swift
//  TaalSp
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import FirebaseAnalytics
import FirebaseDatabase

class RequestHistoryVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    //MARK:- Variable
    lazy var theCurrentView:RequestHistoryView = { [unowned self] in
        return self.view as! RequestHistoryView
        }()
    
    lazy var theCurrentModel: RequestHistoryViewModel = {
        return RequestHistoryViewModel(theController: self)
    }()
    
    var ref : DatabaseReference!
    var selectParentController = enumHistoryParent.fromTab
    
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        theCurrentView.setupUI()
        
        /*self.theCurrentModel.allProviderCategoriesListAPI(categoryID: "0") {
            print("Pending Data")
            self.theCurrentView.tblRequestHistory.reloadData()
        }*/
        
        if isAcceptedCaseNotification {
            isAcceptedCaseNotification = false
            self.theCurrentModel.isshowLoader = false
            self.btnSelectAcceptedAction(self.theCurrentView.btnAcceptedOutlet)
            self.theCurrentModel.CategoryID = "1"
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            obj.setupData(selectedController: .accepted)
            obj.theCurrentModel.categoryID = dictNotificationData["quotation_id"].stringValue
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(obj, animated: false)
        }
        else if isUserConfirmNotification {
            isUserConfirmNotification = false
            self.theCurrentModel.isshowLoader = false
            self.btnSelectAcceptedAction(self.theCurrentView.btnAcceptedOutlet)
            self.theCurrentModel.CategoryID = "1"
            let obj =  self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
            obj.setupData(selectedController: .accepted)
            obj.theCurrentModel.categoryID = dictNotificationData["quotation_id"].stringValue
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.setNavigationBarHidden(false, animated: true)
            self.navigationController?.pushViewController(obj, animated: false)
        }
        else {
            self.theCurrentModel.isshowLoader = true
            if selectParentController == .fromTab {
//                self.theCurrentModel.CategoryID = "0"
                self.btnSelectPendingAction(self.theCurrentView.btnPendingOutlet)
            }
            else {
                self.theCurrentView.heightOfUpperView.constant = 0
                self.theCurrentModel.CategoryID = "3"
                pullToRefresh()
                self.theCurrentView.vwUppar.isHidden = true
            }
        }
        
        self.setUpRefreshController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        setupNavigationbarwithBackButton(titleText: "My_quatation_key".localized.capitalized, barColor: .appThemeBlueColor)
        
//        if selectParentController == .fromTab {
//            //                self.theCurrentModel.CategoryID = "0"
//            self.btnSelectPendingAction(self.theCurrentView.btnPendingOutlet)
//        }
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        if self.theCurrentModel.isProfileVC {
            setupNavigationbarwithShowBackButton(titleText: "My_quatation_key".localized.capitalized, barColor: .appThemeBlueColor, showBackButton: true)
        }
        else {
            setupNavigationbarwithShowBackButton(titleText: "My_quatation_key".localized.capitalized, barColor: .appThemeBlueColor, showBackButton: false)
        }
        
        if selectParentController == .fromProfile {
//            if self.theCurrentModel.CategoryID != "3" {
//                self.theCurrentModel.CategoryID = "3"
//                self.theCurrentModel.isshowLoader = true
//                self.theCurrentModel.allProviderCategoriesListAPI(categoryID: self.theCurrentModel.CategoryID) {
//                    print("Pending Data")
//                    self.theCurrentView.tblRequestHistory.reloadData()
//                }
//            }
        }
        else {
            if isCategoryDelete || isHistoryVC {
                self.btnSelectPendingAction(self.theCurrentView.btnPendingOutlet)
                isCategoryDelete = false
                isHistoryVC = false
            }
            else if isCompletedJob {
                isCompletedJob = false
                self.theCurrentModel.CategoryID = "1"
                self.theCurrentModel.isshowLoader = true
                self.theCurrentModel.allProviderCategoriesListAPI(categoryID: self.theCurrentModel.CategoryID) {
                    print("Pending Data")
                    self.theCurrentView.tblRequestHistory.reloadData()
                }
            }
        }
    }

    func setUpRefreshController() {
        
        self.theCurrentModel.refreshController = UIRefreshControl()
        self.theCurrentModel.refreshController.backgroundColor = UIColor.clear
        self.theCurrentModel.refreshController.tintColor = UIColor.appThemeBlueColor
        self.theCurrentModel.refreshController.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        self.theCurrentView.tblRequestHistory.addSubview(self.theCurrentModel.refreshController)
    }
    
    @objc func pullToRefresh() {
        self.theCurrentModel.allProviderCategoriesListAPI(categoryID: self.theCurrentModel.CategoryID) {
            print("Pending Data")
            self.theCurrentView.tblRequestHistory.reloadData()
        }
    }
}

//MARK:- Action ZOne

extension RequestHistoryVC {
    
    @IBAction func btnSelectPendingAction(_ sender:UIButton){
        theCurrentView.btnSelectPending()
        
        if self.theCurrentModel.CategoryID != "0" {
            self.theCurrentModel.CategoryID = "0"
            self.theCurrentModel.isshowLoader = true
            self.theCurrentModel.allProviderCategoriesListAPI(categoryID: self.theCurrentModel.CategoryID) {
                print("Pending Data")
                self.theCurrentView.tblRequestHistory.reloadData()
            }
        }
    }
    
    @IBAction func btnSelectAcceptedAction(_ sender:UIButton){
        theCurrentView.btnSelectAccepted()
        
        if self.theCurrentModel.CategoryID != "1" {
            self.theCurrentModel.CategoryID = "1"
            self.theCurrentModel.isshowLoader = true
            self.theCurrentModel.allProviderCategoriesListAPI(categoryID: self.theCurrentModel.CategoryID) {
                print("Accepted Data")
                self.theCurrentView.tblRequestHistory.reloadData()
            }
        }
    }
}

//MARK:- Tablview Delegate & Datasource
extension RequestHistoryVC:UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.theCurrentModel.arrayCategoryData.count == 0 {
            setTableView("No_record_found_key".localized, tableView: tableView)
            return 0
        }
        tableView.backgroundView = nil
        return self.theCurrentModel.arrayCategoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "RequestTableCell") as! RequestTableCell
        if theCurrentView.btnAcceptedOutlet.isSelected {
            cell.vwStatus.backgroundColor = .appThemeGreenColor
        } else {
            cell.vwStatus.backgroundColor = .appThemeLightOrangeColor
        }
        
        cell.btnWriteaReview.tag = indexPath.row
        cell.btnWriteaReview.addTarget(self, action: #selector(btnWriteReviewAction(_:)), for: .touchUpInside)
        let data = self.theCurrentModel.arrayCategoryData[indexPath.row]
        cell.setUpData(data: data)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectParentController == .fromProfile {
            return
        }
        let obj =  self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
        if theCurrentView.btnAcceptedOutlet.isSelected {
           obj.setupData(selectedController: .accepted)
        } else{
           obj.setupData(selectedController: .pending)
        }
        
        let data = self.theCurrentModel.arrayCategoryData[indexPath.row]
        obj.theCurrentModel.categoryID = "\(data.quotationId ?? 0)"
        
        obj.theCurrentModel.deleteQuotationHandlor = {
//            self.theCurrentModel.CategoryID = "0"
            
            self.theCurrentModel.arrayCategoryData.remove(at: indexPath.row)
            self.theCurrentView.tblRequestHistory.reloadData()
            
//            self.theCurrentModel.allProviderCategoriesListAPI(categoryID: self.theCurrentModel.CategoryID) {
//                print("Pending Data")
//            }
        }
        
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func btnWriteReviewAction(_ sender: UIButton) {

        let dict = self.theCurrentModel.arrayCategoryData[sender.tag]
        print("Data:-\(dict)")
        let reviewVC = self.storyboard?.instantiateViewController(withIdentifier: "ReviewVC") as! ReviewVC
        reviewVC.mainModelView.caseID = "\(dict.caseId!)"
        self.navigationController?.pushViewController(reviewVC, animated: true)
    }
}

//
//  ReviewVC_TextVCDelegate.swift
//  TaalSp
//
//  Created by Vishal on 22/06/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

extension ReviewVC : UITextViewDelegate {
    
    //MARK: TextView Delegate
    func textViewDidChange(_ textView: UITextView) {
        self.mainView.lblReviewDescription.isHidden = textView.text == "" ? false : true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


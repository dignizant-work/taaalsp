//
//  ReviewView.swift
//  TaalSp
//
//  Created by Vishal on 22/06/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import HCSStarRatingView

class ReviewView: UIView {
    
    //MARK: Outlets
    @IBOutlet var lblTapRat: UILabel!
    @IBOutlet var vwStarRating: HCSStarRatingView!
    @IBOutlet var txtReviewHeadline: UITextField!
    @IBOutlet var lblReviewDescription: UILabel!
    @IBOutlet var txtVwReviewDescription: UITextView!
    @IBOutlet var btnSubmit: CustomButton!
    
    
    //MARK: SetUp
    func setUpUI(theController: ReviewVC) {
        
        [lblTapRat].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 24, fontname: .semi)
        }
        
        [txtReviewHeadline].forEach { (txt) in
            txt?.font = themeFont(size: 14, fontname: .regular)
            txt?.textColor = UIColor.appThemeBlueColor
            txt?.attributedPlaceholder = NSAttributedString(string: "Review_headline_key".localized.capitalized, attributes: [NSAttributedString.Key.foregroundColor: UIColor.appThemeBlueColor])
        }
        
        [lblReviewDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
        [txtVwReviewDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlueColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        lblTapRat.text = "Tap_a_rate_key".localized
        lblReviewDescription.text = "Review_description_key".localized
    }
    
}

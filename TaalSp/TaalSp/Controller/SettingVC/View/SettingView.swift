//
//  SettingView.swift
//  Taal
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SettingView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblSetting: UITableView!
    
    func setUpUI(theController: SettingVC) {
        tblSetting.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        
        var dic = JSON()
        dic["img"].stringValue = "ic_notification_setting"
        dic["title"].stringValue = "Notifications_key".localized
        dic["isNotificationCell"].stringValue = "true"
        dic["isSelected"].stringValue = "true"
        theController.mainViewModel.arraySetting.append(dic)
    }
    
}

//
//  SignupVC.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import RSSelectionMenu
//import Firebase
import FirebaseAuth

class SignupVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:SignUpView = { [unowned self] in
        return self.view as! SignUpView
        }()
    
    lazy var theCurrentModel: SignupViewModel = {
        return SignupViewModel(theController: self)
    }()
    
    let dataArray = ["Sachin Tendulkar", "Rahul Dravid", "Saurav Ganguli", "Virat Kohli", "Suresh Raina", "Ravindra Jadeja", "Chris Gyle", "Steve Smith", "Anil Kumble"]
    var selectedDataArray = [String]()
    var cellSelectionStyle: CellSelectionStyle = .tickmark

    //MARK:- ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        printFonts()
        self.theCurrentView.registerXib(theDelegate: self)
        self.theCurrentModel.categoryList {
            self.theCurrentView.tblCategory.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupTransparentNavigationBar()
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_white")?.imageFlippedForRightToLeftLayoutDirection() , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .white
        
        self.navigationItem.leftBarButtonItems = [leftButton]
    }    
   
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        theCurrentView.setupUI(theDelegate: self)
    }

}

//MARK:- Action Zone

extension SignupVC {
    
    @IBAction func btnSignupAction(_ sender:UIButton){
        
        if (self.theCurrentView.txtUsername.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_username_key".localized)
        }
        else if (self.theCurrentView.txtEmailAddress.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_email_key".localized)
        }
        else if (self.theCurrentView.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_phone_number_key".localized)
        }
        else if self.theCurrentView.txtMobileNumber.text?.isNumeric == false {
            makeToast(strMessage: "Please_enter_valid_phone_number_key".localized)
        }
        else if self.theCurrentModel.arrayCategoryID.count == 0 {
            makeToast(strMessage: "Please_select_any_one_category_key".localized)
        }
        else if (self.theCurrentView.btnTermsOutlet.isSelected == false) {
            makeToast(strMessage: "Please_accept_Terms_of_Service_key".localized)
        }
        else {
            let dict = ["mobile" : self.theCurrentView.txtMobileNumber.text ?? "",
                        "country_code" : self.theCurrentModel.countryCode,
                        "email" : self.theCurrentView.txtEmailAddress.text ?? ""] as [String : Any]
            print("Param:- \(dict)")
            self.theCurrentModel.signupDataDict = JSON(dict)
            self.theCurrentModel.signupDataDict["fullname"].stringValue = self.theCurrentView.txtUsername.text!
            self.theCurrentModel.checkIsMobileEmailExist(param: dict) {
                
                self.setupMobileNumberOTP()
                
//                let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
//                otpVC.theControllerModel.signupDataDict = self.theCurrentModel.signupDataDict
//                otpVC.theControllerModel.arrayCategoryID = self.theCurrentModel.arrayCategoryID
//                self.navigationController?.pushViewController(otpVC, animated: true)
            }
        }
    }
    
    @IBAction func btnSelectTermsConditionAction(_ sender:UIButton){
        self.view.endEditing(true)
        theCurrentView.btnSelectAccept(sender)
    }
}

// MARK:- CountryCode Delegate

extension SignupVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        
        print("data:\(data)")
        self.theCurrentModel.countryCode = data["dial_code"].stringValue
        self.theCurrentView.btnSelectCountryOutlet.setTitle(data["dial_code"].stringValue, for: .normal)
        
    }
    
}

//MARK:- Action Zone

extension SignupVC {
    
    @IBAction func btnSelectCountryAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }
    
    @IBAction func btnCategoriesShowAction(_ sender: Any) {
        
        self.theCurrentView.vwCategories.isHidden = true
    }

}

//MARK:- UITextfield Delegate

extension SignupVC:UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == theCurrentView.txtCategories {
            [self.theCurrentView.txtUsername, self.theCurrentView.txtMobileNumber, self.theCurrentView.txtEmailAddress, self.theCurrentView.txtCategories].forEach { (txt) in
                txt?.resignFirstResponder()
            }
            DispatchQueue.main.async {
                self.theCurrentView.vwCategories.isHidden = false
            }
//            showAsMultiSelectPopover(sender: textField)
//            showWithCustomCell(sender: textField)
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.theCurrentView.txtUsername {
            textField.resignFirstResponder()
            self.theCurrentView.txtEmailAddress.becomeFirstResponder()
        }
        else if textField == self.theCurrentView.txtEmailAddress {
            textField.resignFirstResponder()
            self.theCurrentView.txtMobileNumber.becomeFirstResponder()
        }
        return true
    }
}

//MARK: OTP Setup
extension SignupVC {
    
    func setupMobileNumberOTP() {
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        let phoneNumber = "\(self.theCurrentModel.countryCode)"+"\(self.theCurrentView.txtMobileNumber.text!)"
        print("phoneNumber: \(phoneNumber)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                print("verificationId: ",verificationId!)
                guard let verify = verificationId else { return }
                userDefault.set(verify, forKey: "verificationId")
                let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                otpVC.theControllerModel.verifyId = verify
                otpVC.theControllerModel.selectController = .register
                otpVC.theControllerModel.signupDataDict = self.theCurrentModel.signupDataDict
                otpVC.theControllerModel.arrayCategoryID = self.theCurrentModel.arrayCategoryID
                self.navigationController?.pushViewController(otpVC, animated: false)
                
            }
            else {
                print("Error: \(error?.localizedDescription ?? "")")
            }
        }
    }
    
}


//--Not used virani setup
//MARK:- RSSelection
extension SignupVC{
    func showAsMultiSelectPopover(sender: UIView) {
        
        let cellNibName = "CategoriesCell"
        let cellIdentifier = "CategoriesCell"
                
        let selectionMenu = RSSelectionMenu(selectionStyle: .multiple, dataSource: dataArray, cellType: .customNib(nibName: cellNibName, cellIdentifier: cellIdentifier)) { (cell, name, indexPath) in
            
            let customCell = cell as! CategoriesCell
         
            customCell.lblCategoriesName.text = name.components(separatedBy: " ").first
            customCell.separatorInset = .zero
            customCell.selectionStyle = .none
            cell.tintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }
        
        selectionMenu.tableView?.layer.borderColor = UIColor.black.cgColor
        selectionMenu.tableView?.layer.borderWidth = 2.0
        selectionMenu.tableView?.layer.cornerRadius = 5
        
//        selectionMenu.view.layer.borderColor = UIColor.black.cgColor
//        selectionMenu.view.layer.borderWidth = 1
        
        selectionMenu.tableView?.separatorColor = .lightGray
        
        selectionMenu.searchBar?.isHidden = true
        
        selectionMenu.setSelectedItems(items: selectedDataArray) { [weak self] (text, index, selected, selectedList) in
            
           
            self?.selectedDataArray = selectedList
         
            self?.theCurrentView.txtCategories.text = selectedList.joined(separator: ", ")
//            self?.tableView.reloadData()
        }
    
        selectionMenu.showEmptyDataLabel(text: "")
      
        selectionMenu.cellSelectionStyle = self.cellSelectionStyle
        
        selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
    }
   
}


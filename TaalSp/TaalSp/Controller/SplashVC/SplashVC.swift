//
//  SplashVC.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    
    override func viewDidLoad() {
        super.viewDidLoad()

        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TourVC") as! TourVC
        self.navigationController?.pushViewController(obj, animated: false)
       
        // Do any additional setup after loading the view.
    }
  
}

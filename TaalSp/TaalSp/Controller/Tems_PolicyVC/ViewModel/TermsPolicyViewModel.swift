//
//  TermsPolicyViewModel.swift
//  Taal
//
//  Created by Abhay on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import WebKit

class TermsPolicyViewModel {
    
    fileprivate weak var theController : TermsPolicyVC!
    var isTerms = Bool()
    var isTitile = String()
    var webView = WKWebView()
    //MARK: Initializer
    init(theController: TermsPolicyVC) {
        self.theController = theController
    }
    
        
    //MARK: GetProfile
    func termsAndPrivacyApi(param: [String:Any] ,completionHandler: @escaping (JSON)-> Void) {
        
        let url = termsAndPrivacyURL
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            
            if let data = result {
                let modelData = JSON(data)
                completionHandler(modelData)
            }
            else if error != nil {
                makeToast(strMessage: error!)
            }
        }
    }
    
}

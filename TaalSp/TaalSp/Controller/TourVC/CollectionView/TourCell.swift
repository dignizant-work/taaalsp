//
//  TourCell.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class TourCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var imgTour: UIImageView!
    
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.textColor = UIColor.appThemeBlueColor
        lblTitle.font = themeFont(size: 30, fontname: .regular)
        
        lblSubTitle.textColor = UIColor.appThemeBlueColor
        lblSubTitle.font = themeFont(size: 20, fontname: .regular)
        lblSubTitle.alpha = 0.5
    }

}

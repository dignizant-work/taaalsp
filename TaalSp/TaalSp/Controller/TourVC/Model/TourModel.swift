//
//  TourModel.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
//import AlamofireSwiftyJSON

class TourModel: NSObject {
    
    //MARK:- Variable
    fileprivate weak var theController:TourVC!
//    var arrTour:[JSON] = []
    var currentIndex:Int = 0
    var arrTour : [TourDataModel] = []
    
    //MARK:- LifeCycle
    init(theController:TourVC) {
        self.theController = theController
    }
    
    func tourDataApi(completionHandler: @escaping ()-> Void) {
        
        let url = screenBase+appType
        let parameters = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
        
            self.theController.hideLoader()
            
            if let data = result as? NSArray {
                userDefault.set(true, forKey: isFirstTime)
                print("Result:-\(result)")
                
                let modelData = JSON(data)
                
                for data in modelData.arrayValue {
                    print("Data:-\(data)")
                    let value = TourDataModel(JSON: data.dictionaryObject!)
                    self.arrTour.append(value!)
                }
                completionHandler()
            }
            else if error != nil {
                makeToast(strMessage: error!)
            }
            
            print("Model:\(self.arrTour)")
            print("Error:-\(error)")
            print("statuscode:-\(statusCode)")
        }
    }
    
}

//MARK:- Setup Array

extension TourModel {
    /*
    func setupArray(){
        arrTour = []
        
        var dict = JSON()
        dict["title"] = "What is Lorem Ipsum?"
        dict["sub_title"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,"
        dict["img"].stringValue = "ic_slider_image_one"
        arrTour.append(dict)
        
        dict = JSON()
        dict["title"] = "Why do we use it?"
        dict["sub_title"] = "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."
        dict["img"].stringValue = "ic_slider_image_two"
        arrTour.append(dict)
        
        dict = JSON()
        dict["title"] = "Where does it come from?"
        dict["sub_title"] = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC"
        dict["img"].stringValue = "ic_slider_image_three"
        arrTour.append(dict)
        
//        dict = JSON()
//        dict["title"] = "Where does it come from?"
//        dict["sub_title"] = "Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC"
//        arrTour.append(dict)
//        
    }
 */
}

//
//  CommonDateTimeVC.swift
//  Rent A SKI
//
//  Created by Haresh on 31/05/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import UIKit


public protocol DateTimePickerDelegate {
    func setDateandTime(dateValue: Date, type: Int)
}

class CommonDateTimeVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var vwColor          : UIView!
    @IBOutlet weak var dateTimePicker   : UIDatePicker!
    @IBOutlet weak var btnCancel        : UIButton!
    @IBOutlet weak var btnDone          : UIButton!
    
    
    //MARK:- Variable
    
    var pickerDelegate : DateTimePickerDelegate?
    var controlType : Int = 0 //0 = Time / 1 = Date / 2 = Datetime
    
    var setMinimumDate : Date?
    var setMaximumDate : Date?

    var isSetMinimumDate : Bool = false
    var isSetMaximumDate : Bool = false

    var isSetDatePickerDate = false
    var datePickerDateValue : Date?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        vwColor.backgroundColor = UIColor.appThemeBlueColor
        btnDone.setTitle("Done_key".localized, for: .normal)
        btnCancel.setTitle("Cancel_key".localized, for: .normal)
        if isSetMinimumDate{
            if setMinimumDate != nil{
                dateTimePicker.minimumDate = setMinimumDate ?? Date()
            }
        }
        
        if isSetMaximumDate{
            if setMaximumDate != nil{
                dateTimePicker.maximumDate = setMaximumDate ?? Date()
            }
        }
        
        if controlType == 0 {
            dateTimePicker.datePickerMode = .time
        }
        else if controlType == 1 {
            dateTimePicker.datePickerMode = .date
        }
        else if controlType == 2 {
            dateTimePicker.datePickerMode = .dateAndTime
        }
        
    }
    
    
    //MARK:- Button Action
    
    @IBAction func btnCancelAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDoneAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        pickerDelegate?.setDateandTime(dateValue: dateTimePicker.date, type: controlType)
    }
    
}


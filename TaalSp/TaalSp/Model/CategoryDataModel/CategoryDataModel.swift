import Foundation 
import ObjectMapper 

class CategoryDataModel: Mappable { 

	var caseId: NSNumber? 
	var categoryName: String? 
	var categoryImage: String? 
	var status: String = ""
    var statusId = Int()
	var hasAudio: NSNumber?
    var offeredPrice: String = ""
    var voiceNotes: String = ""
    var extraNotes:String = ""
    var quotationId: NSNumber?
    var isDelete: Bool = true
    var isEdit: Bool = true
    var price : String = ""
    var categoryId = NSNumber()
    var voiceUrl : String = ""
    var audioBase64 = String()
    var quotationExtraNotes:String = ""
    var latitude : Double = 0.0
    var longitude : Double = 0.0
	var caseDescription: [CaseDescription] = []
    var userDetails: UserDescription?
    var isAdd: String = ""
    

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
        
		caseId <- map["case_id"] 
		categoryName <- map["category_name"] 
		categoryImage <- map["category_image"] 
		status <- map["status"] 
		hasAudio <- map["hasAudio"] 
		caseDescription <- map["case_description"]
        userDetails <- map["user_details"]
        offeredPrice <- map["offered_price"]
        voiceNotes <- map["voice_notes"]
        extraNotes <- map["extra_notes"]
        quotationId <- map["quotation_id"]
        isDelete <- map["is_delete"]
        isEdit <- map["is_edit"]
        price <- map["price"]
        categoryId <- map["category_id"]
        voiceUrl <- map["voiceUrl"]
        audioBase64 <- map["audioBase64"]
        quotationExtraNotes <- map["quotationExtraNotes"]
        latitude <- map["latitude"]
        longitude <- map["longtitude"]
        statusId <- map["status_id"]
        isAdd <- map["isAdd"]
	}
} 

class CaseDescription: Mappable { 

    var type: String?
	var label: String? 
	var value: [String]? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
        type <- map["type"]
		label <- map["label"] 
		value <- map["value"] 
	}
} 

class UserDescription: Mappable {
    
    var name: String?
    var address: String?
    var conversationId:String = ""
    var mobile:String = ""
    var countryCode = ""
    var userID : NSNumber = 0
    var rating : NSNumber = 0
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        address <- map["address"]
        conversationId <- map["conversation_id"]
        mobile <- map["mobile"]
        countryCode <- map["country_code"]
        userID <- map["userID"]
        rating <- map["rating"]
    }
}

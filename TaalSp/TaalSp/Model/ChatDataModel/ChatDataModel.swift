import Foundation 
import ObjectMapper 

class ChatDataModel: Mappable { 

	var date: String?
    var chatDict: Chat?
	var chat: [Chat]?
    var userID: NSNumber?
    var fullname: String?
    var conversationId:String = ""

	convenience required init?(map: Map){
        self.init()
	} 

	func mapping(map: Map) {
		date <- map["date"]
        chatDict <- map["chat"]
		chat <- map["chat"]
        userID <- map["userID"]
        fullname <- map["fullname"]
        conversationId <- map["conversation_id"]        
	}
} 

class Chat: Mappable { 

	var message: String? 
	var time: String? 
	var reply: Bool? 

	 convenience required init?(map: Map){
        self.init()
    }

	func mapping(map: Map) {
		message <- map["message"] 
		time <- map["time"] 
		reply <- map["reply"] 
	}
} 


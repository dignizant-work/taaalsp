import Foundation 
import ObjectMapper 

class LoginDataModel: Mappable {

	var accessToken: String? 
	var tokenType: String? 
	var user: User? 

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		accessToken <- map["access_token"] 
		tokenType <- map["token_type"] 
		user <- map["user"] 
	}
} 

class User: Mappable {

	var rating: NSNumber? 
	var playerId: String? 
	var email: String? 
	var imgThumb: String? 
	var countryId: NSNumber? 
	var phone: Any? 
	var status: NSNumber? 
	var approved: Bool? 
	var imageUrl: Any? 
	var id: NSNumber? 
	var mobile: String? 
	var fullname: String?
    var countryCode: String?
    var notificationSetting: String = ""

	required init?(map: Map){ 
	} 

	func mapping(map: Map) {
		rating <- map["rating"] 
		playerId <- map["player_id"] 
		email <- map["email"] 
		imgThumb <- map["img_thumb"] 
		countryId <- map["country_id"] 
		phone <- map["phone"] 
		status <- map["status"] 
		approved <- map["approved"] 
		imageUrl <- map["imageUrl"] 
		id <- map["id"] 
		mobile <- map["mobile"] 
		fullname <- map["fullname"]
        countryCode <- map["country_code"]
        notificationSetting <- map["notification_setting"]
	}
} 


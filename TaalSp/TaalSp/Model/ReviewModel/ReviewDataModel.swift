//
//  ReviewDataModel.swift
//  TaalSp
//
//  Created by Vishal on 23/06/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import ObjectMapper

class ReviewDataModel: Mappable {

    var rating: NSNumber = 0
    var title: String = ""
    var comment: String = ""
    var userName: String = ""

    required init?(map: Map){
    }

    func mapping(map: Map) {
        rating <- map["rating"]
        title <- map["title"]
        comment <- map["comment"]
        userName <- map["user_name"]
    }
}


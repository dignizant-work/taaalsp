import Foundation
import ObjectMapper

class TourDataModel: Mappable {
    
    var id: NSNumber?
    var name: String?
    var description: String?
    var imageUrl: String?
    var pageNumber: NSNumber?
    var isSubCat: Int?
    var tokenType: String?
    var accessToken: String?
    var message: String?
    var isSelected: Int = 0
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        imageUrl <- map["imageUrl"]
        pageNumber <- map["pageNumber"]
        isSubCat <- map["isSubCat"]
        tokenType <- map["token_type"]
        accessToken <- map["access_token"]
        message <- map["message"]
        isSelected <- map["isSelected"]
    }
}

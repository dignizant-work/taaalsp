//
//  Constants.swift
//  TaalSp
//
//  Created by Vishal on 16/12/19.
//  Copyright © 2019 Vishal. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


//MARK: Push response
let createCase          = "create"          //add case
let acceptedCase        = "accepted"        //quotation accepted
let completedJob        = "completed"       //job completed
let cancelledJob        = "cancelled"       //job cancelled
let updateCase          = "update"          //case update
let deleteCase          = "delete"          //case delete
let startJob            = "start job"       //job started
let arrivedAtLocation   = "arrived"         //provider arrived at location
let startTracking       = "tracking"        //start tracking
let userConfirm         = "confirm"         //user confirmed


let preferredLanguage = NSLocale.preferredLanguages[0] // get system device language

//Notification Variables
var isNotificationChat = false
var isAcceptedCaseNotification = false
var isUserConfirmNotification = false

var dictNotificationData = JSON()
var handlorNewCreateCase:()->Void = {}
var isJobCompleted = false
var isCompletedJob = false
var isHistoryVC = false




///--- User data ---
func getUserData() -> LoginDataModel? {
    if let user = userDefault.object(forKey: user_data_key) as? Data, let json = JSON(user).dictionaryObject {
        return LoginDataModel(JSON: json)
    }
    return nil
}

func setValueUserDetail(forKey: String ,forValue: String) {
    
    guard let userDetail = UserDefaults.standard.value(forKey: user_data_key) as? Data else { return }
    var data = JSON(userDetail)
    data["user"][forKey].stringValue = forValue
    
    guard let rowdata = try? data.rawData() else {return}
    userDefault.setValue(rowdata, forKey: user_data_key)
    userDefault.synchronize()
}

func savedUserData(user: JSON) {
    guard let rawData = try? user.rawData() else { return }
    userDefault.set(rawData, forKey: user_data_key)
    userDefault.synchronize()
}

func removeUserData() {
    userDefault.removeObject(forKey: user_data_key)
    userDefault.synchronize()
}


func updateUserDicData(user: JSON) {
    guard let userDetail = UserDefaults.standard.value(forKey: user_data_key) as? Data else { return }
    var data = JSON(userDetail)
    data["user"] = user
    
    guard let rowdata = try? data.rawData() else {return}
    userDefault.setValue(rowdata, forKey: user_data_key)
    userDefault.synchronize()
}


//MARK: - Reload Extensions
extension UITableView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

extension UICollectionView {
    func reloadData(completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: { self.reloadData() })
        { _ in completion() }
    }
}

//MARK: File Manager path
extension FileManager {
    
    class func directoryUrl() -> URL? {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths.first
    }
    
    class func allRecordedData() -> [URL]? {
        if let documentsUrl = FileManager.directoryUrl() {
            do {
                let directoryContents = try FileManager.default.contentsOfDirectory(at: documentsUrl, includingPropertiesForKeys: nil)
                return directoryContents.filter{ $0.pathExtension == "m4a" }
            } catch {
                return nil
            }
        }
        return nil
    }
    
}

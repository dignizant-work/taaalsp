//
//  Date + Extension.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation

extension Date{
    
    func generateDatesArrayBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date) ->[Date]
    {
        var datesArray: [Date] =  [Date]()
        var startDate = startDate
        let calendar = Calendar.current
        
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        
        while startDate <= endDate {
            datesArray.append(startDate)
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return datesArray
    }
    
    func DatesAvailableBetweenTwoDates(OrinalDtFormater:String,startDate: Date , endDate:Date,SelectedDate:Date) ->Bool
    {
        var startDate = startDate
        let calendar = Calendar.current
        let fmt = DateFormatter()
        fmt.dateFormat = OrinalDtFormater
        while startDate <= endDate
        {
            if(startDate == SelectedDate)
            {
                return true
            }
            startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
            
        }
        return false
    }
    
    
    
    
    
}



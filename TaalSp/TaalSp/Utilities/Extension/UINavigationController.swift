//
//  UINavigationController.swift
//  TaalSp
//
//  Created by Jaydeep on 04/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {

    func getReferenceVC<ViewController: UIViewController>(to viewController: ViewController.Type) -> ViewController? {
        return viewControllers.first { $0 is ViewController } as? ViewController
    }
}
